Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Scrap Entry")> 
<Assembly: AssemblyDescription("a product of Bridge Software Technologies")> 
<Assembly: AssemblyCompany("Reading Rock Inc")> 
<Assembly: AssemblyProduct("Scrap Entry")> 
<Assembly: AssemblyCopyright("copyright 2008")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("4FAA241A-C2D2-431F-A1A4-387F255D8C7A")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("5.0.0.2")> 

<Assembly: ComVisibleAttribute(False)> 