Public Class frmJoScrapRpt
    Inherits System.Windows.Forms.Form
    Public sJO As String
    Friend WithEvents DsPS As ScrapEntry.dsPS
    Friend WithEvents ReportDataBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ReportDataTableAdapter As ScrapEntry.dsPSTableAdapters.ReportDataTableAdapter
    Friend WithEvents TableAdapterManager As ScrapEntry.dsPSTableAdapters.TableAdapterManager
    Private rpt As JoScrap

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents crvMain As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJoScrapRpt))
        Me.crvMain = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.DsPS = New ScrapEntry.dsPS
        Me.ReportDataBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReportDataTableAdapter = New ScrapEntry.dsPSTableAdapters.ReportDataTableAdapter
        Me.TableAdapterManager = New ScrapEntry.dsPSTableAdapters.TableAdapterManager
        CType(Me.DsPS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportDataBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvMain
        '
        Me.crvMain.ActiveViewIndex = -1
        Me.crvMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvMain.Location = New System.Drawing.Point(0, 0)
        Me.crvMain.Name = "crvMain"
        Me.crvMain.SelectionFormula = ""
        Me.crvMain.Size = New System.Drawing.Size(712, 429)
        Me.crvMain.TabIndex = 0
        Me.crvMain.ViewTimeSelectionFormula = ""
        '
        'DsPS
        '
        Me.DsPS.DataSetName = "dsPS"
        Me.DsPS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportDataBindingSource
        '
        Me.ReportDataBindingSource.DataMember = "ReportData"
        Me.ReportDataBindingSource.DataSource = Me.DsPS
        '
        'ReportDataTableAdapter
        '
        Me.ReportDataTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.app_errorsTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.ht_prodTableAdapter = Nothing
        Me.TableAdapterManager.m2mFac_ps_xrefTableAdapter = Nothing
        Me.TableAdapterManager.scrap_codesTableAdapter = Nothing
        Me.TableAdapterManager.scrapTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ScrapEntry.dsPSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'frmJoScrapRpt
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(712, 429)
        Me.Controls.Add(Me.crvMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmJoScrapRpt"
        Me.ShowInTaskbar = False
        Me.Text = "Scrap by JO"
        CType(Me.DsPS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportDataBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmJoScrapRpt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rpt = New JoScrap
        rpt.FileName = Application.StartupPath & "\JoScrap.rpt"
        Dim dt As New dsPS.ReportDataDataTable
        dt = ReportDataTableAdapter.GetReportDataByJobno(sJO)
        Dim ds As New DataSet
        ds.Tables.Add(dt)
        'rpt.SetDataSource(ds)
        crvMain.ReportSource = rpt
    End Sub

    Private Sub GetReportDataByJobnoToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub
End Class
