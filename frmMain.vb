Imports System.Data.SqlClient

Public Class Form1
    Inherits System.Windows.Forms.Form
#Region "< Members >"
    Private bJo As Boolean
    Private bQty As Boolean
    Private bCode As Boolean
    Dim nday As Integer = Now.Date.Day
    Dim dStart As Date = Now.Date.AddDays(-nday)
    Dim nProd As Decimal
    Dim nScrap As Decimal
    Dim sFac As String = ""
    Dim sCell As String = ""
    Dim dvScrap As New DataView
    Dim dvHtProd As New DataView
    Dim dvJobSelect As New DataView
    Dim nProdQty As Integer = 0
#End Region
#Region "< Friend WithEvents >"
    Friend WithEvents DsPS As ScrapEntry.dsPS
    Friend WithEvents App_errorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents App_errorsTableAdapter As ScrapEntry.dsPSTableAdapters.app_errorsTableAdapter
    Friend WithEvents TableAdapterManager As ScrapEntry.dsPSTableAdapters.TableAdapterManager
    Friend WithEvents M2mFac_ps_xrefTableAdapter As ScrapEntry.dsPSTableAdapters.m2mFac_ps_xrefTableAdapter
    Friend WithEvents M2mFac_ps_xrefBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Ht_prodTableAdapter As ScrapEntry.dsPSTableAdapters.ht_prodTableAdapter
    Friend WithEvents Ht_prodBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InmastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InmastTableAdapter As ScrapEntry.dsPSTableAdapters.inmastTableAdapter
    Friend WithEvents JomastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents JomastTableAdapter As ScrapEntry.dsPSTableAdapters.jomastTableAdapter
    Friend WithEvents ScrapTableAdapter As ScrapEntry.dsPSTableAdapters.scrapTableAdapter
    Friend WithEvents ScrapBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Scrap_codesTableAdapter As ScrapEntry.dsPSTableAdapters.scrap_codesTableAdapter
    Friend WithEvents Scrap_codesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sum_scrapBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sum_scrapTableAdapter As ScrapEntry.dsPSTableAdapters.sum_scrapTableAdapter
    Friend WithEvents cbxFac As System.Windows.Forms.ComboBox
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
#End Region
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents gbxScrap As System.Windows.Forms.GroupBox
    Friend WithEvents lblJO As System.Windows.Forms.Label
    Friend WithEvents txtJoSearch As System.Windows.Forms.TextBox
    Friend WithEvents cbxCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblPartno As System.Windows.Forms.Label
    Friend WithEvents lblJobName As System.Windows.Forms.Label
    Friend WithEvents lblDescript As System.Windows.Forms.Label
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents lblQty As System.Windows.Forms.Label
    'Friend WithEvents DsScrap1 As ScrapEntry.dsSrap
    Friend WithEvents cmdEnter As System.Windows.Forms.Button
    Friend WithEvents cbxDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtJoReport As System.Windows.Forms.TextBox
    Friend WithEvents lblJoReport As System.Windows.Forms.Label
    Friend WithEvents lblRunningTotal As System.Windows.Forms.Label
    Friend WithEvents lblProdCl As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAboutScrap As System.Windows.Forms.MenuItem
    Friend WithEvents lblScrapCodeEng As System.Windows.Forms.Label
    Friend WithEvents lblScrapCodeSp As System.Windows.Forms.Label
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents lblProduced As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.gbxScrap = New System.Windows.Forms.GroupBox
        Me.cbxFac = New System.Windows.Forms.ComboBox
        Me.M2mFac_ps_xrefBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsPS = New ScrapEntry.dsPS
        Me.lblProduced = New System.Windows.Forms.Label
        Me.lblScrapCodeEng = New System.Windows.Forms.Label
        Me.Scrap_codesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cmdEnter = New System.Windows.Forms.Button
        Me.lblQty = New System.Windows.Forms.Label
        Me.txtQty = New System.Windows.Forms.TextBox
        Me.lblDescript = New System.Windows.Forms.Label
        Me.JomastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblJobName = New System.Windows.Forms.Label
        Me.lblPartno = New System.Windows.Forms.Label
        Me.cbxCode = New System.Windows.Forms.ComboBox
        Me.txtJoSearch = New System.Windows.Forms.TextBox
        Me.lblJO = New System.Windows.Forms.Label
        Me.cbxDateTime = New System.Windows.Forms.DateTimePicker
        Me.lblProdCl = New System.Windows.Forms.Label
        Me.lblScrapCodeSp = New System.Windows.Forms.Label
        Me.txtJoReport = New System.Windows.Forms.TextBox
        Me.lblJoReport = New System.Windows.Forms.Label
        Me.lblRunningTotal = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuExit = New System.Windows.Forms.MenuItem
        Me.mnuAbout = New System.Windows.Forms.MenuItem
        Me.mnuAboutScrap = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.lblSummary = New System.Windows.Forms.Label
        Me.App_errorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.App_errorsTableAdapter = New ScrapEntry.dsPSTableAdapters.app_errorsTableAdapter
        Me.TableAdapterManager = New ScrapEntry.dsPSTableAdapters.TableAdapterManager
        Me.Ht_prodTableAdapter = New ScrapEntry.dsPSTableAdapters.ht_prodTableAdapter
        Me.M2mFac_ps_xrefTableAdapter = New ScrapEntry.dsPSTableAdapters.m2mFac_ps_xrefTableAdapter
        Me.Scrap_codesTableAdapter = New ScrapEntry.dsPSTableAdapters.scrap_codesTableAdapter
        Me.ScrapTableAdapter = New ScrapEntry.dsPSTableAdapters.scrapTableAdapter
        Me.Ht_prodBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InmastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InmastTableAdapter = New ScrapEntry.dsPSTableAdapters.inmastTableAdapter
        Me.JomastTableAdapter = New ScrapEntry.dsPSTableAdapters.jomastTableAdapter
        Me.ScrapBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sum_scrapBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sum_scrapTableAdapter = New ScrapEntry.dsPSTableAdapters.sum_scrapTableAdapter
        Me.gbxScrap.SuspendLayout()
        CType(Me.M2mFac_ps_xrefBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Scrap_codesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JomastBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.App_errorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ht_prodBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InmastBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ScrapBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sum_scrapBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxScrap
        '
        Me.gbxScrap.Controls.Add(Me.cbxFac)
        Me.gbxScrap.Controls.Add(Me.lblProduced)
        Me.gbxScrap.Controls.Add(Me.lblScrapCodeEng)
        Me.gbxScrap.Controls.Add(Me.cmdEnter)
        Me.gbxScrap.Controls.Add(Me.lblQty)
        Me.gbxScrap.Controls.Add(Me.txtQty)
        Me.gbxScrap.Controls.Add(Me.lblDescript)
        Me.gbxScrap.Controls.Add(Me.lblJobName)
        Me.gbxScrap.Controls.Add(Me.lblPartno)
        Me.gbxScrap.Controls.Add(Me.cbxCode)
        Me.gbxScrap.Controls.Add(Me.txtJoSearch)
        Me.gbxScrap.Controls.Add(Me.lblJO)
        Me.gbxScrap.Controls.Add(Me.cbxDateTime)
        Me.gbxScrap.Controls.Add(Me.lblProdCl)
        Me.gbxScrap.Controls.Add(Me.lblScrapCodeSp)
        Me.gbxScrap.Location = New System.Drawing.Point(8, 8)
        Me.gbxScrap.Name = "gbxScrap"
        Me.gbxScrap.Size = New System.Drawing.Size(476, 189)
        Me.gbxScrap.TabIndex = 0
        Me.gbxScrap.TabStop = False
        Me.gbxScrap.Text = "Scrap Entry"
        '
        'cbxFac
        '
        Me.cbxFac.DataSource = Me.M2mFac_ps_xrefBindingSource
        Me.cbxFac.DisplayMember = "psshortdesc"
        Me.cbxFac.FormattingEnabled = True
        Me.cbxFac.Location = New System.Drawing.Point(16, 26)
        Me.cbxFac.Name = "cbxFac"
        Me.cbxFac.Size = New System.Drawing.Size(96, 21)
        Me.cbxFac.TabIndex = 15
        Me.cbxFac.ValueMember = "psshortdesc"
        '
        'M2mFac_ps_xrefBindingSource
        '
        Me.M2mFac_ps_xrefBindingSource.DataMember = "m2mFac_ps_xref"
        Me.M2mFac_ps_xrefBindingSource.DataSource = Me.DsPS
        '
        'DsPS
        '
        Me.DsPS.DataSetName = "dsPS"
        Me.DsPS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblProduced
        '
        Me.lblProduced.Location = New System.Drawing.Point(128, 117)
        Me.lblProduced.Name = "lblProduced"
        Me.lblProduced.Size = New System.Drawing.Size(80, 16)
        Me.lblProduced.TabIndex = 12
        '
        'lblScrapCodeEng
        '
        Me.lblScrapCodeEng.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Scrap_codesBindingSource, "description_eng", True))
        Me.lblScrapCodeEng.Location = New System.Drawing.Point(96, 141)
        Me.lblScrapCodeEng.Name = "lblScrapCodeEng"
        Me.lblScrapCodeEng.Size = New System.Drawing.Size(144, 16)
        Me.lblScrapCodeEng.TabIndex = 10
        Me.lblScrapCodeEng.Text = "English"
        Me.lblScrapCodeEng.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Scrap_codesBindingSource
        '
        Me.Scrap_codesBindingSource.DataMember = "scrap_codes"
        Me.Scrap_codesBindingSource.DataSource = Me.DsPS
        '
        'cmdEnter
        '
        Me.cmdEnter.Location = New System.Drawing.Point(376, 157)
        Me.cmdEnter.Name = "cmdEnter"
        Me.cmdEnter.Size = New System.Drawing.Size(75, 23)
        Me.cmdEnter.TabIndex = 9
        Me.cmdEnter.Text = "Enter"
        '
        'lblQty
        '
        Me.lblQty.Location = New System.Drawing.Point(16, 119)
        Me.lblQty.Name = "lblQty"
        Me.lblQty.Size = New System.Drawing.Size(24, 16)
        Me.lblQty.TabIndex = 8
        Me.lblQty.Text = "Qty"
        Me.lblQty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(56, 117)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(56, 20)
        Me.txtQty.TabIndex = 7
        '
        'lblDescript
        '
        Me.lblDescript.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fdescript", True))
        Me.lblDescript.Location = New System.Drawing.Point(224, 77)
        Me.lblDescript.Name = "lblDescript"
        Me.lblDescript.Size = New System.Drawing.Size(232, 24)
        Me.lblDescript.TabIndex = 6
        Me.lblDescript.Text = "Description"
        Me.lblDescript.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'JomastBindingSource
        '
        Me.JomastBindingSource.DataMember = "jomast"
        Me.JomastBindingSource.DataSource = Me.DsPS
        '
        'lblJobName
        '
        Me.lblJobName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fjob_name", True))
        Me.lblJobName.Location = New System.Drawing.Point(224, 109)
        Me.lblJobName.Name = "lblJobName"
        Me.lblJobName.Size = New System.Drawing.Size(232, 32)
        Me.lblJobName.TabIndex = 5
        Me.lblJobName.Text = "JobName"
        Me.lblJobName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPartno
        '
        Me.lblPartno.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fpartno", True))
        Me.lblPartno.Location = New System.Drawing.Point(224, 53)
        Me.lblPartno.Name = "lblPartno"
        Me.lblPartno.Size = New System.Drawing.Size(184, 16)
        Me.lblPartno.TabIndex = 4
        Me.lblPartno.Text = "Partno"
        Me.lblPartno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxCode
        '
        Me.cbxCode.DataSource = Me.Scrap_codesBindingSource
        Me.cbxCode.DisplayMember = "scrap_code"
        Me.cbxCode.Location = New System.Drawing.Point(16, 149)
        Me.cbxCode.Name = "cbxCode"
        Me.cbxCode.Size = New System.Drawing.Size(56, 21)
        Me.cbxCode.TabIndex = 3
        Me.cbxCode.ValueMember = "scrap_code"
        '
        'txtJoSearch
        '
        Me.txtJoSearch.Location = New System.Drawing.Point(56, 85)
        Me.txtJoSearch.Name = "txtJoSearch"
        Me.txtJoSearch.Size = New System.Drawing.Size(56, 20)
        Me.txtJoSearch.TabIndex = 2
        '
        'lblJO
        '
        Me.lblJO.Location = New System.Drawing.Point(16, 87)
        Me.lblJO.Name = "lblJO"
        Me.lblJO.Size = New System.Drawing.Size(32, 16)
        Me.lblJO.TabIndex = 1
        Me.lblJO.Text = "JO"
        Me.lblJO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDateTime
        '
        Me.cbxDateTime.Location = New System.Drawing.Point(16, 53)
        Me.cbxDateTime.Name = "cbxDateTime"
        Me.cbxDateTime.Size = New System.Drawing.Size(192, 20)
        Me.cbxDateTime.TabIndex = 0
        '
        'lblProdCl
        '
        Me.lblProdCl.Location = New System.Drawing.Point(128, 85)
        Me.lblProdCl.Name = "lblProdCl"
        Me.lblProdCl.Size = New System.Drawing.Size(40, 16)
        Me.lblProdCl.TabIndex = 11
        Me.lblProdCl.Text = "ProdCl"
        '
        'lblScrapCodeSp
        '
        Me.lblScrapCodeSp.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Scrap_codesBindingSource, "description_sp", True))
        Me.lblScrapCodeSp.Location = New System.Drawing.Point(96, 165)
        Me.lblScrapCodeSp.Name = "lblScrapCodeSp"
        Me.lblScrapCodeSp.Size = New System.Drawing.Size(144, 16)
        Me.lblScrapCodeSp.TabIndex = 10
        Me.lblScrapCodeSp.Text = "Spanish"
        Me.lblScrapCodeSp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJoReport
        '
        Me.txtJoReport.Location = New System.Drawing.Point(136, 201)
        Me.txtJoReport.Name = "txtJoReport"
        Me.txtJoReport.Size = New System.Drawing.Size(56, 20)
        Me.txtJoReport.TabIndex = 1
        '
        'lblJoReport
        '
        Me.lblJoReport.Location = New System.Drawing.Point(8, 200)
        Me.lblJoReport.Name = "lblJoReport"
        Me.lblJoReport.Size = New System.Drawing.Size(120, 23)
        Me.lblJoReport.TabIndex = 2
        Me.lblJoReport.Text = "Enter JO to get report"
        Me.lblJoReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRunningTotal
        '
        Me.lblRunningTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRunningTotal.Location = New System.Drawing.Point(235, 200)
        Me.lblRunningTotal.Name = "lblRunningTotal"
        Me.lblRunningTotal.Size = New System.Drawing.Size(237, 23)
        Me.lblRunningTotal.TabIndex = 3
        Me.lblRunningTotal.Text = "running total"
        Me.lblRunningTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.MenuItem1, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuExit})
        Me.mnuFile.Text = "&File"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 0
        Me.mnuExit.Text = "E&xit"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAboutScrap})
        Me.mnuAbout.Text = "&About"
        '
        'mnuAboutScrap
        '
        Me.mnuAboutScrap.Index = 0
        Me.mnuAboutScrap.Text = "A&bout Scrap Entry"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 1
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Admin"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem3, Me.MenuItem4})
        Me.MenuItem2.Text = "Reconfig DB"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 0
        Me.MenuItem3.Text = "M2M"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "Packing Solution"
        '
        'lblSummary
        '
        Me.lblSummary.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSummary.Location = New System.Drawing.Point(0, 243)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(495, 26)
        Me.lblSummary.TabIndex = 4
        Me.lblSummary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'App_errorsBindingSource
        '
        Me.App_errorsBindingSource.DataMember = "app_errors"
        Me.App_errorsBindingSource.DataSource = Me.DsPS
        '
        'App_errorsTableAdapter
        '
        Me.App_errorsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.app_errorsTableAdapter = Me.App_errorsTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ht_prodTableAdapter = Me.Ht_prodTableAdapter
        Me.TableAdapterManager.m2mFac_ps_xrefTableAdapter = Me.M2mFac_ps_xrefTableAdapter
        Me.TableAdapterManager.scrap_codesTableAdapter = Me.Scrap_codesTableAdapter
        Me.TableAdapterManager.scrapTableAdapter = Me.ScrapTableAdapter
        Me.TableAdapterManager.UpdateOrder = ScrapEntry.dsPSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Ht_prodTableAdapter
        '
        Me.Ht_prodTableAdapter.ClearBeforeFill = True
        '
        'M2mFac_ps_xrefTableAdapter
        '
        Me.M2mFac_ps_xrefTableAdapter.ClearBeforeFill = True
        '
        'Scrap_codesTableAdapter
        '
        Me.Scrap_codesTableAdapter.ClearBeforeFill = True
        '
        'ScrapTableAdapter
        '
        Me.ScrapTableAdapter.ClearBeforeFill = True
        '
        'Ht_prodBindingSource
        '
        Me.Ht_prodBindingSource.DataMember = "ht_prod"
        Me.Ht_prodBindingSource.DataSource = Me.DsPS
        '
        'InmastBindingSource
        '
        Me.InmastBindingSource.DataMember = "inmast"
        Me.InmastBindingSource.DataSource = Me.DsPS
        '
        'InmastTableAdapter
        '
        Me.InmastTableAdapter.ClearBeforeFill = True
        '
        'JomastTableAdapter
        '
        Me.JomastTableAdapter.ClearBeforeFill = True
        '
        'ScrapBindingSource
        '
        Me.ScrapBindingSource.DataMember = "scrap"
        Me.ScrapBindingSource.DataSource = Me.DsPS
        '
        'Sum_scrapBindingSource
        '
        Me.Sum_scrapBindingSource.DataMember = "sum_scrap"
        Me.Sum_scrapBindingSource.DataSource = Me.DsPS
        '
        'Sum_scrapTableAdapter
        '
        Me.Sum_scrapTableAdapter.ClearBeforeFill = True
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(495, 269)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.lblRunningTotal)
        Me.Controls.Add(Me.lblJoReport)
        Me.Controls.Add(Me.txtJoReport)
        Me.Controls.Add(Me.gbxScrap)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "Scrap Entry"
        Me.gbxScrap.ResumeLayout(False)
        Me.gbxScrap.PerformLayout()
        CType(Me.M2mFac_ps_xrefBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Scrap_codesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JomastBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.App_errorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ht_prodBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InmastBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ScrapBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sum_scrapBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        My.Settings.sCell = sCell
        My.Settings.sFac = sFac

        My.Settings.Save()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim sqlconn As New SqlClient.SqlConnection
            sqlconn.ConnectionString = My.Settings.M2MDATA01ConnectionString
            sqlconn.Open()
            sqlconn.Close()
            sqlconn.Dispose()
        Catch ex As Exception
            ' if we cannot connect, then display the SqlConnWiz
            Dim results As DialogResult
            SQLConnWizard.Text = "Configure connection to M2M"
            results = SQLConnWizard.ShowDialog(Me)
            If results = Windows.Forms.DialogResult.OK Then
                My.Settings.m2mConnection = SQLConnWizard.sConnection
                My.Settings.Save()
            Else
                MsgBox("You did not create a successful connection to M2M.", MsgBoxStyle.Exclamation, "Shutting down..")
                End
            End If
        End Try

        Try
            Dim sqlconn As New SqlClient.SqlConnection
            sqlconn.ConnectionString = My.Settings.Packing_SolutionConnectionString
            sqlconn.Open()
            sqlconn.Close()
            sqlconn.Dispose()
        Catch ex As Exception
            ' if we cannot connect, then display the SqlConnWiz
            Dim results As DialogResult
            SQLConnWizard.Text = "Configure connection to Packing Solution"
            results = SQLConnWizard.ShowDialog(Me)
            If results = Windows.Forms.DialogResult.OK Then
                My.Settings.psConnection = SQLConnWizard.sConnection
                My.Settings.Save()
            Else
                MsgBox("You did not create a successful connection to Packing Solution.", MsgBoxStyle.Exclamation, "Shutting down..")
                End
            End If
        End Try
        'TODO: This line of code loads data into the 'DsPS.m2mFac_ps_xref' table. You can move, or remove it, as needed.
        Me.M2mFac_ps_xrefTableAdapter.FillFacXref(Me.DsPS.m2mFac_ps_xref)

        M2mFac_ps_xrefBindingSource.Position = M2mFac_ps_xrefBindingSource.Find("psshortdesc", My.Settings.sCell)
        Try
            ScrapTableAdapter.FillScrap(DsPS.scrap)
            sFac = My.Settings.sFac
            sCell = My.Settings.sCell

            Scrap_codesTableAdapter.FillScrapCodes(DsPS.scrap_codes)
            Ht_prodTableAdapter.FillHTProd(DsPS.ht_prod, sFac)
            dvScrap.Table = DsPS.scrap
            dvHtProd.Table = DsPS.ht_prod
            dvJobSelect.Table = DsPS.jomast

            'TODO: This line of code loads data into the 'DsPS.scrap_codes' table. You can move, or remove it, as needed.
            Me.Scrap_codesTableAdapter.FillScrapCodes(Me.DsPS.scrap_codes)
        Catch ex As Odbc.OdbcException
            App_errorsTableAdapter.Insert(cbxFac.SelectedValue.ToString.Trim, "Scrap Entry", ex.Message.ToString(), System.Environment.UserName, Now)
            MsgBox(ex.Message.ToString, MsgBoxStyle.Information, "Scrap Tables")
        End Try

        bJo = False
        bQty = False
        bCode = False
        cmdEnter.Enabled = False
        calc_scrap()
        cbxDateTime.MaxDate = Now.Date

    End Sub
    Private Sub cmdEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEnter.Click
        Dim sJO As String = txtJoSearch.Text

        If Not txtJoSearch.Text.Contains("-0000") Then
            sJO = txtJoSearch.Text.Trim + "-0000"
        End If
        sJO = sJO.ToUpper.Trim

        Try
            ScrapTableAdapter.Insert(cbxDateTime.Value.Date, sJO, lblPartno.Text, Integer.Parse(txtQty.Text), cbxCode.SelectedValue, 0, sFac, sCell)
            Ht_prodTableAdapter.Insert(9, 0, sJO, dvHtProd(0)("cuft"), Integer.Parse(txtQty.Text) * -1, cbxDateTime.Value.Date, cbxDateTime.Value.Date, False, Now, dvHtProd(0)("fac"), dvHtProd(0)("fsono"), dvHtProd(0)("finumber"), dvHtProd(0)("frelease"), sFac)
        Catch ex As SqlException
            App_errorsTableAdapter.Insert(cbxFac.SelectedValue.ToString.Trim, "Scrap Entry - EnterClick: ", ex.Message.ToString(), System.Environment.UserName, Now)
        End Try

        Dim da As New dsPSTableAdapters.QueriesTableAdapter()

        Dim sQty As String = da.SumByJobno(sJO).ToString()
        '  Tell the user how many have been scrapped thus far for the job.
        lblRunningTotal.Text = txtJoSearch.Text & " --  " & sQty & "  Total scrapped."

        txtJoSearch.Text = ""
        txtJoSearch.Focus()
        txtQty.Text = ""
        lblProduced.Text = ""
        bJo = False
        bQty = False
        bCode = False
        cmdEnter.Enabled = False
        calc_scrap()
    End Sub

    Private Sub txtQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        If IsNumeric(txtQty.Text) Then
            ' filter schedule
            If (CType(txtQty.Text, Integer) > 0 And CType(txtQty.Text, Integer) <= nProdQty) Then
                bQty = True
                AllPassed()
            Else
                If CType(txtQty.Text, Integer) > nProdQty Then
                    MsgBox("You can not scrap more than has been produced", MsgBoxStyle.Information, "Over Scrapping???")
                    txtQty.Text = ""
                End If
            End If
        Else
            'Beep()
            txtQty.Text = ""
        End If

    End Sub

    Private Sub txtJoSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJoSearch.TextChanged
        Dim sFullJOName As String
        dvJobSelect.Sort = "fjobno"
        dvJobSelect.RowFilter = Nothing


        If txtJoSearch.TextLength >= 5 Then
            If txtJoSearch.TextLength = 5 Then
                sFullJOName = txtJoSearch.Text & "-0000"
            Else
                sFullJOName = Trim(txtJoSearch.Text)
            End If
            nProdQty = get_jo_info(sFullJOName.ToUpper)
            lblProduced.Text = nProdQty & " made."

            'MsgBox(nRowCount)
            'MsgBox(M2Mdataview.Item(nRowCount).Item("fjobno"))
            If dvJobSelect.Count = 1 And nProdQty > 0 Then
                dvJobSelect.RowFilter = "fjobno = '" & sFullJOName & "'"
                bJo = True
                AllPassed()
                txtQty.Focus()
            Else
                MsgBox(txtJoSearch.Text & " not found. Jo has to be in RELEASED status.", MsgBoxStyle.Information, "Looking for JO's")
                txtJoSearch.Text = ""
                txtJoSearch.Focus()
            End If
        End If
    End Sub
    Private Sub AllPassed()
        If bQty And bJo And bCode Then
            cmdEnter.Enabled = True
        End If
    End Sub

    Private Sub cbxCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCode.SelectedIndexChanged
        bCode = True
        AllPassed()
    End Sub

    Private Sub txtJoReport_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJoReport.TextChanged
        Dim sFullJOName As String
        Dim nRowCount As Integer
        dvScrap.Sort = "fjobno"
        dvScrap.RowFilter = Nothing


        If txtJoReport.TextLength >= 5 Then
            If txtJoReport.TextLength = 5 Then
                sFullJOName = txtJoReport.Text & "-0000"
            Else
                sFullJOName = Trim(txtJoReport.Text)
            End If
            nRowCount = dvScrap.Find(sFullJOName)
            'MsgBox(nRowCount)
            'MsgBox(M2Mdataview.Item(nRowCount).Item("fjobno"))
            If nRowCount <> -1 Then
                Dim objReport As New frmJoScrapRpt
                objReport.sJO = sFullJOName
                objReport.ShowDialog(Me)
                txtJoReport.Text = ""
                txtJoReport.Focus()
            Else
                MsgBox(txtJoReport.Text & " not found. No scrap entered.", MsgBoxStyle.Information, "Looking for JO's")
                txtJoReport.Text = ""
                txtJoReport.Focus()
            End If
        End If

    End Sub
    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Form1_FormClosing(Nothing, Nothing)
        End
    End Sub
    Private Sub mnuAboutScrap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAboutScrap.Click
        ShowAboutBox()
    End Sub
    Public Sub ShowAboutBox()
        Dim objAboutBox As New About
        objAboutBox.ShowDialog(Me)
    End Sub
    Private Sub calc_scrap()
        Dim daHtTotSQL As New dsPSTableAdapters.scrap_rate_by_cellTableAdapter
        Dim dt As New dsPS.scrap_rate_by_cellDataTable
        dt = daHtTotSQL.GetData(sCell)
        Dim nRate As Decimal
        If dt(0).tot_prod > 0 Then
            nRate = dt(0).tot_scrapped / dt(0).tot_prod
        Else
            nRate = 0
        End If
        lblSummary.Text = "CUFT Produced:  " & Format(dt(0).tot_prod, "####.0") & "  CuFt Scrapped:  " & Format(dt(0).tot_scrapped, "####.0") & "  Scrap is:  " & Format(nRate * 100, "##.00") & "%"
    End Sub
    Private Function get_jo_info(ByVal jo As String) As Integer

        Dim nSumQty As Integer = 0

        Try
            JomastTableAdapter.Fill(DsPS.jomast, sFac, jo)
            Ht_prodTableAdapter.FillByJobNoPFac(DsPS.ht_prod, jo, sFac)

            dvHtProd.Table = DsPS.ht_prod
            For Each dr As DataRowView In dvHtProd
                nSumQty += dr("qty_prod")
            Next
            dvHtProd.RowFilter = Nothing
            Return nSumQty
            'Return dvFnusrqty1.Item(0).Item("fnusrqty1")
        Catch ex As SqlClient.SqlException
            MsgBox(ex.Message.ToString, MsgBoxStyle.Information, "M2M tables")
        End Try

    End Function

    Private Sub cbxDateTime_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxDateTime.GotFocus
        cbxDateTime.MaxDate = Now.Date
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Dim sConnArr As String() = My.Settings.M2MDATA01ConnectionString.ToString.Split(";")
        For Each el As String In sConnArr
            Dim aEl As String() = el.Split("=")
            If el.Contains("Catalog") Then
                MsgBox("M2M catalog set to: " & aEl(1))
                Exit For
            End If
        Next
        Dim results As DialogResult
        SQLConnWizard.Text = "Configure connection to M2M"
        results = SQLConnWizard.ShowDialog(Me)
        If results = Windows.Forms.DialogResult.OK Then
            My.Settings.m2mConnection = SQLConnWizard.sConnection
            My.Settings.Save()
        Else
            MsgBox("You did not create a successful connection to M2M.", MsgBoxStyle.Exclamation, "Shutting down..")
            End
        End If
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim sConnArr As String() = My.Settings.Packing_SolutionConnectionString.ToString.Split(";")
        For Each el As String In sConnArr
            Dim aEl As String() = el.Split("=")
            If el.Contains("Catalog") Then
                MsgBox("Packing Solution catalog set to: " & aEl(1))
                Exit For
            End If
        Next
        Dim results As DialogResult
        SQLConnWizard.Text = "Configure connection to M2M"
        results = SQLConnWizard.ShowDialog(Me)
        If results = Windows.Forms.DialogResult.OK Then
            My.Settings.m2mConnection = SQLConnWizard.sConnection
            My.Settings.Save()
        Else
            MsgBox("You did not create a successful connection to M2M.", MsgBoxStyle.Exclamation, "Shutting down..")
            End
        End If
    End Sub

    Private Sub cbxFac_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxFac.SelectionChangeCommitted
        sCell = cbxFac.SelectedValue.ToString.Trim
        M2mFac_ps_xrefBindingSource.Position = M2mFac_ps_xrefBindingSource.Find("psshortdesc", sCell)
        If sFac <> M2mFac_ps_xrefBindingSource.Current(1).ToString.Trim Then
            sFac = M2mFac_ps_xrefBindingSource.Current(1).ToString.Trim
        End If
        calc_scrap()
    End Sub
End Class
